The David Strasser Office loves to help people with their auto, home, life and business insurance in Mill Creek, WA. They do that by being different then other agencies. They will do the little things others wont or don't do.

Address: 1700 132nd St SE, UNIT H, Mill Creek, WA 98012, USA

Phone: 425-347-4685

Website: https://strasserinsurance.com